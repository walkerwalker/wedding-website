const express = require('express');
const app = express();
const cors = require('cors')
app.use(cors());

const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));


//signedUrl get endpoint
const getSignedUrl = require('./getSignedUrl'); 
app.get('/signedUrl', getSignedUrl);


//rsvp post endpoint
const postRsvp = require('./postRsvp'); 
app.post('/rsvp', postRsvp);

module.exports = app;