const AWS = require('aws-sdk')
AWS.config.update({ region: process.env.REGION })
const s3 = new AWS.S3();

//todo change bucket destinatioon
const uploadBucket = 'yiyunjiamin-wedding-website/uploaded-photos'   

//todo add test
async function getSignedUrl(req, res) {
    const randomId = parseInt(Math.random()*1000000000);

    const s3Params = {
        Bucket: uploadBucket,
        Key:  `${randomId}.jpg`,
        ContentType: 'image/jpeg',
        ACL: 'public-read',   
    };

    // Get signed URL from s3
    try {
        const uploadURL = s3.getSignedUrl('putObject', s3Params)
        const response = {
            "uploadURL": uploadURL,
            "photoFilename": `${randomId}.jpg`
        };
        res.status(200).send(response);    
    } catch(error) {
        res.status(500).send("error: " + error.message);
    }
}

module.exports = getSignedUrl;