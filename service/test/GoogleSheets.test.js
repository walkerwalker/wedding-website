const {append, deleteRows, getSheetProperties, A1RangeToGridRange, getValues, authenticate} = require("../GoogleSheets");

const sheetId = 0; //corresponding to sheetName Guests
const sheetName = "Guests";
const rows = [[1,1,1,1,1],[2,2,2,2,2],[3,3,3,3,3]];
let rowCount;
let columnCount;
let updatedRange;

describe('test suite for using google sheets api', () => {
    //TODO more test for column AA, or one side empty
    it('A1Range to GridRange', () => {
        const A1Range = 'Guests!A2:E4'
        const gridRange = A1RangeToGridRange(A1Range);
        expect(gridRange).toEqual({
            startRow: 1, 
            endRow: 3, 
            startCol: 0, 
            endCol: 4,
            name: "Guests"
        })
    });

    it('authenticate', async () => {
        await authenticate();
    });

    it('getSheetProperties', async () => {
        const properties = await getSheetProperties();
        expect(properties.sheetId).toBe(sheetId);
        expect(properties.title).toBe(sheetName);

        //for comparasion after deletion
        rowCount = properties.gridProperties.rowCount;
        columnCount = properties.gridProperties.columnCount;
    });

    it('append', async () => {
        const response = await append(rows);
        expect(response.status).toBe(200);

        expect(response.data.updates.updatedRows).toBe(3);
        expect(response.data.updates.updatedColumns).toBe(5);
        let values = response.data.updates.updatedData.values;
        values = values.map( row => row.map( y => parseInt(y))); //deep copy
        expect(values).toEqual(rows);

        //save for deletion
        updatedRange = response.data.updates.updatedRange;
    });

    it('getValues', async () => {
        const response = await getValues(updatedRange);
        expect(response.status).toBe(200);

        values = response.data.values;
        values = values.map( row => row.map( y => parseInt(y))); //deep copy
        expect(values).toEqual(rows);
    });

    it('deleteRows', async () => {
        const response = await deleteRows(updatedRange);
        expect(response.status).toBe(200);

        const gridProperties = response.data.updatedSpreadsheet.sheets[0].properties.gridProperties;
        expect(gridProperties.rowCount).toBe(rowCount);
        expect(gridProperties.columnCount).toBe(columnCount);
    });
})
