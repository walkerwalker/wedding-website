const request = require('supertest')
const app = require('../app')

describe('test suite for getSignedUrl api',  () => {
    it('get signedUrl', async () => {
        const response = await request(app).get("/signedUrl");
        expect(response.status).toBe(200);
        expect(response.body.uploadURL).not.toEqual(null);
        expect(response.body.photoFilename).not.toEqual(null);
    });
})
