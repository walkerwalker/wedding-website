const { response } = require('express');
const request = require('supertest')
const app = require('../app')
const {deleteRows} = require("../GoogleSheets");

describe('test suite for postRsvp endpoint',  () => {
    it('post rsvp come', async () => {
        formData = {
            "name_1": "jiamin",
            "name_2": "jiamin",
            "name_3": "jiamin",
            "name_4": "jiamin",
            "name_5": "jiamin",
            "diet_1": "none",
            "diet_2": "none",
            "diet_3": "none",
            "diet_4": "none",
            "diet_5": "none",
            "email_1": "j@j.com",
            "come_1": "yes",
            "plus_1": "4",
            "message": "ohyeah",   
        }
        const rsvpData = [
            [ 'jiamin', 'j@j.com', 'yes', 'none', '4', 'ohyeah' ],
            [ 'jiamin', 'j@j.com', 'yes', 'none' ],
            [ 'jiamin', 'j@j.com', 'yes', 'none' ],
            [ 'jiamin', 'j@j.com', 'yes', 'none' ],
            [ 'jiamin', 'j@j.com', 'yes', 'none' ]
        ];
        const response = await request(app).post("/rsvp").type('form').send(formData);
        expect(response.status).toBe(200);
        expect(response.body.updates.updatedRows).toBe(5);
        expect(response.body.updates.updatedColumns).toBe(6);

        let values = response.body.updates.updatedData.values;        
        expect(values).toEqual(rsvpData);

        //assume this is tested.
        await deleteRows(response.body.updates.updatedRange);
    });


    it('post rsvp not come', async () => {
        formData = {
            "name_1": "jiamin",
            "name_2": "",
            "name_3": "",
            "name_4": "",
            "name_5": "",
            "diet_1": "",
            "diet_2": "",
            "diet_3": "",
            "diet_4": "",
            "diet_5": "",
            "email_1": "j@j.com",
            "come_1": "no",
            "plus_1": "",
            "message": "ohyeah",   
        }
        const rsvpData = [
            [ 'jiamin', 'j@j.com', 'no', '', '', 'ohyeah' ],
        ];
        const response = await request(app).post("/rsvp").type('form').send(formData);
        expect(response.status).toBe(200);
        expect(response.body.updates.updatedRows).toBe(1);
        expect(response.body.updates.updatedColumns).toBe(6);

        let values = response.body.updates.updatedData.values;        
        expect(values).toEqual(rsvpData);

        //assume this is tested.
        await deleteRows(response.body.updates.updatedRange);
    });
})
