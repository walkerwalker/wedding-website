const {google} = require('googleapis');
const { request } = require('./app');
const sheets = google.sheets('v4')

const SCOPES = ['https://www.googleapis.com/auth/spreadsheets']

const spreadsheetId = '1zdCZPuTpbnTgo9DoGpGI9U_W2fdy_OpHF-THhp46tnc';
const sheetName = 'Guests';

async function authenticate() {
    process.env.GOOGLE_APPLICATION_CREDENTIALS = './service_account_credentials.json';

    const auth = new google.auth.GoogleAuth({
        scopes: SCOPES
    });
    const authClient = await auth.getClient();
    google.options({auth:authClient});
}

async function getSheetProperties() {
    await authenticate();

    const response = await sheets.spreadsheets.get({
        spreadsheetId,
        ranges: [sheetName],
    });

    return response.data.sheets[0].properties;
}

async function append(rowsData) {
    await authenticate();

    const response = await sheets.spreadsheets.values.append({
        spreadsheetId,
        range: sheetName,
        valueInputOption: "USER_ENTERED",
        includeValuesInResponse: true,
        resource: {
            values: rowsData
        }
    });

    return response;
}

async function getValues(range) {
    await authenticate();

    const response = await sheets.spreadsheets.values.get({
        spreadsheetId,
        range: range,
    });

    return response;
}

async function deleteRows(range) {
    await authenticate();

    const gridRange = A1RangeToGridRange(range);
    const sheetProperties = await getSheetProperties();
    const requests = [];
    requests.push({
        "deleteDimension": {
            "range": {
                "sheetId": sheetProperties.sheetId,
                "dimension": "ROWS",
                "startIndex": gridRange.startRow,
                "endIndex": gridRange.endRow +1
            }
        } 
    });
    
    const response = await sheets.spreadsheets.batchUpdate({
        spreadsheetId: spreadsheetId,
        resource: {
            requests: requests,
            includeSpreadsheetInResponse: true
        }
    });

    return response;
}

//TODO handle column like AA, handle one side is none
function A1RangeToGridRange(nameA1Range){
    const name = nameA1Range.split("!")[0];
    const rangeInA1 = nameA1Range.split("!")[1];
    const startRowCol = rangeInA1.split(":")[0];
    const endRowCol = rangeInA1.split(":")[1];

    let startCol = startRowCol.slice(0, startRowCol.search(/\d/));
    let startRow = startRowCol.replace(startCol, '');
    let endCol = endRowCol.slice(0, endRowCol.search(/\d/));
    let endRow = endRowCol.replace(endCol, '');

    startCol = startCol.charCodeAt(0) - 'A'.charCodeAt(0);
    endCol = endCol.charCodeAt(0) - 'A'.charCodeAt(0);
    startRow = startRow - 1;
    endRow = endRow -1;

    return {name, startRow, endRow, startCol, endCol};

    // consider using this gist
    // https://gist.github.com/tanaikech/95c7cd650837f33a564babcaf013cae0
}

module.exports = {append, deleteRows, getSheetProperties, A1RangeToGridRange, getValues, authenticate}