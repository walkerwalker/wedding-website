const {append} = require('./GoogleSheets.js');


async function postRsvp(req, res) {
    const name_1 = req.body.name_1;
    const name_2 = req.body.name_2;
    const name_3 = req.body.name_3;
    const name_4 = req.body.name_4;
    const name_5 = req.body.name_5;
    const diet_1 = req.body.diet_1; 
    const diet_2 = req.body.diet_2;
    const diet_3 = req.body.diet_3;
    const diet_4 = req.body.diet_4;
    const diet_5 = req.body.diet_5;
    const email_1 = req.body.email_1;
    const come_1 = req.body.come_1;
    const plus_1 = req.body.plus_1;
    const message = req.body.message;

    const p0 = [name_1, email_1, come_1, diet_1, plus_1, message];
    const p1 = [name_2, email_1, come_1, diet_2, "", ""];
    const p2 = [name_3, email_1, come_1, diet_3, "", ""];
    const p3 = [name_4, email_1, come_1, diet_4, "", ""];
    const p4 = [name_5, email_1, come_1, diet_5, "", ""];

    const numPlus = parseInt(plus_1) || 0;
    const rsvpData = [p0,p1,p2,p3,p4].slice(0, numPlus+1);

    try {
        const response = await append(rsvpData);
        res.status(200).send(response.data);
    } catch (error) {
        res.status(500).send("error: " + error.message);
    }    
}

module.exports = postRsvp;