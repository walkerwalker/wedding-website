const app = require("./app");

const awsServerlessExpress = require('aws-serverless-express');
const server = awsServerlessExpress.createServer(app);

exports.handler = (event, context) => { 
    awsServerlessExpress.proxy(server, event, context) 
}

// todo replace with serverless-offline
// app.listen(3000, () => console.log("listening on port 3000..."));