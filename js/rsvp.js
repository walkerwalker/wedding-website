$(document).ready(function () {

    /********************** RSVP **********************/

    const waitMsg = {
        'en': 'Just a sec! We are saving the information',
        'cn': '稍等，我们正在保存您的回复。'
    };

    const successMsg = {
        'en': 'Thank you. We have received your reply. If you have any questions, drop us an email at <a href="mailto:hello@yiyunjiamin.com">hello@yiyunjiamin.com</a> or I am sure you know how to reach us. ^_^',
        'cn': '谢谢。我们已经收到了您的回复。如果有任何问题，可以发邮件至 <a href="mailto:hello@yiyunjiamin.com">hello@yiyunjiamin.com</a>  或者您肯定知道如何联系到我们 ^_^'
    };

    const failMsg = {
        'en': 'Sorry! Something went wrong.',
        'cn': '抱歉！出了点状况。'
    };
    
    const lambdaUrl = "LAMBDA_URL_TO_BE_INJECTED_AT_DEPLOY";
    const stage = "STAGE_NAME_TO_BE_INJECTED_AT_DEPLOY";
    const rsvpEndpoint = "rsvp";
    
    $('#rsvp-form').on('submit', function (e) {
        e.preventDefault(); //no idea why it should be here

        // determining the languange by text on button, a bit ugly
        let lang;
        let buttonText = $('.lang').text();
        if (buttonText === 'Let Us Know') {
            lang = 'en';
        } else {
            lang = 'cn';
        };

        $('#alert-wrapper').html(alert_markup(waitMsg[lang]));

        const formData = $(this).serialize();
        $.ajax ({
            type: "POST",
            url: lambdaUrl + '/' + stage + '/' + rsvpEndpoint,
            data: formData,
        }).done(function (response) {
            console.log("ok");
            console.log(response);
            $('#alert-wrapper').html(alert_markup(successMsg[lang]));
        }).fail(function (error) {
            console.log('failed');
            console.log(error);
            $('#alert-wrapper').html(alert_markup(failMsg[lang]));
        });
    });


});

/********************** Extras **********************/

// alert_markup
function alert_markup(msg) {
    return '<div class="message-body">' + msg + '</div>';
}


/********************** Logic form **********************/
function hideExtraNames() {
    for (i = 1; i <= 4; i++) {
        document.getElementById('name' + i).style.display = "none";
    }
}

function displayNameAndMore(n) {
  console.log(n);

  hideExtraNames();

  for (i = 1; i <= parseInt(n); i++) {
      document.getElementById('name' + i).style.display = "block";
  }

}

function displayQuestion(answer) {
  console.log(answer);

  if (answer == "go") { // hide the div that is not selected

    document.getElementById('ifgo').style.display = "block";
    // hideExtraNames();
    // console.log(document.getElementById('plusones').value);
    displayNameAndMore(document.getElementById('plusones').value);
  } else if (answer == "nogo") {

    document.getElementById('ifgo').style.display = "none";
    hideExtraNames();
  }
}