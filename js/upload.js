const MAX_IMAGE_SIZE = 10000000
const lambdaUrl = "LAMBDA_URL_TO_BE_INJECTED_AT_DEPLOY";
const stage = "STAGE_NAME_TO_BE_INJECTED_AT_DEPLOY";
const signedUrlEndpoint = "signedUrl";

const API_ENDPOINT = lambdaUrl + '/' + stage + '/' + signedUrlEndpoint;

new Vue({
  el: "#app",
  data: {
    image: '',
    uploadURL: ''
  },
  methods: {
    onFileChange (e) {
      let files = e.target.files || e.dataTransfer.files
      if (!files.length) return
      this.createImage(files[0])
    },
    createImage (file) {
      // var image = new Image()
      let reader = new FileReader()
      reader.onload = (e) => {
        if (!e.target.result.includes('data:image/jpeg')) {
          return alert('Wrong file type - JPG only.')
        }
        if (e.target.result.length > MAX_IMAGE_SIZE) {
          return alert('Image is loo large.')
        }
        this.image = e.target.result
      }
      reader.readAsDataURL(file)
    },
    removeImage: function (e) {
      this.image = ''
    },
    uploadImage: async function (e) {
      // Get the presigned URL
      const response = await axios({
        method: 'GET',
        url: API_ENDPOINT
      })
      let binary = atob(this.image.split(',')[1])
      let array = []
      for (var i = 0; i < binary.length; i++) {
        array.push(binary.charCodeAt(i))
      }
      let blobData = new Blob([new Uint8Array(array)], {type: 'image/jpeg'})
      const result = await fetch(response.uploadURL, {
        method: 'PUT',
        body: blobData
      })
      // Final URL for the user doesn't need the query string params
      this.uploadURL = response.uploadURL.split('?')[0]
    }
  }
})